﻿using System;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class DarkMode : MonoBehaviour
{

    [SerializeField]
    private GameObject _darkModePrefab;
    [SerializeField]
    private Toggle _switch;
    [SerializeField]
    private Image _logo;
    public static bool _darkMode;
    private TMP_Text[] _Texts;

    private void Start()
    {
        _Texts = FindObjectsOfType<TMP_Text>();

    }

    void Update()
    {

        if (_switch.isOn)
            _darkMode = true;
        else
            _darkMode = false;

        if (_darkMode == true)
        {
            Activate();
        }
        else
        {
            Deactivate();
        }
    }

    private void Deactivate()
    {

        _darkModePrefab.SetActive(false);
        _logo.color = Color.black;
        for (int i = 0; i < _Texts.Length; i++)
        {

            _Texts[i].color = Color.black;
        }
    }

    private void Activate()
    {

        _darkModePrefab.SetActive(true);
        _logo.color = Color.white;
        for (int i = 0; i < _Texts.Length; i++)
        {

            _Texts[i].color = Color.white;
        }
    }


}
