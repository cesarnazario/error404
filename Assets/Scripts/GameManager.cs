﻿using System.Collections;
using UnityEngine;
using TMPro;


public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _windowPrefab;
    [SerializeField]
    private TMP_Text _timeDisplay;
    [SerializeField]
    private TMP_Text _finalScoreDisplay;
    [SerializeField]
    private TMP_Text _scoreDisplay;
    [SerializeField]
    private GameObject _gameOverPanel;

    private Vector3 _spawnPosition;

    //-5.5f, 14.5f 
    private float _xPosLimit = 14.5f;
    private float _xNegLimit = -5.5f;
    //-3.2f, 9.5f
    private float _yPosLimit = 9.5f;
    private float _yNegLimit = -3.2f;

    private float _spawnInterval = 2f;
    private float _timeRemaining = 96f; //96 duration of the background music

    public static int _score;

    private AudioSource _backgroundMusic;

    private void Start()
    {
        StartCoroutine(SpawnRoutine());
        _score = 0;
        _backgroundMusic = GetComponent<AudioSource>();
    }


    IEnumerator SpawnRoutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(_spawnInterval);
            PopUpSpawn();
        }
    }
    private void Update()
    {
        TimeCount();
        _scoreDisplay.text = "Score: " + _score.ToString();

        if (_timeRemaining <= 0)
            GameOver();

    }
    private void GameOver()
    {
        Time.timeScale = 0;
        _gameOverPanel.gameObject.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        _backgroundMusic.Stop();
        _finalScoreDisplay.text = "Score: " + _score.ToString();
    }
    private void TimeCount()
    {
        if (_timeRemaining > 0)
        {
            _timeRemaining -= Time.deltaTime;
        }

        var _minutes = Mathf.FloorToInt(_timeRemaining / 60);
        var _seconds = Mathf.FloorToInt(_timeRemaining % 60);

        _timeDisplay.text = string.Format("Time: {0:00}:{1:00}", _minutes, _seconds);

    }

    private void PopUpSpawn()
    {
        _spawnPosition = new Vector3(UnityEngine.Random.Range(_xNegLimit, _xPosLimit), UnityEngine.Random.Range(_yNegLimit, _yPosLimit), 0); //I identify the limits where a spawned window does not go out of screen
        Instantiate(_windowPrefab, _spawnPosition, new Quaternion());
    }


}
