﻿using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float _moveSpeed = 8f;

    [SerializeField]
    private float _turnSpeed = 90f;

    [SerializeField]
    private float _rayDistance = 1f;
    [SerializeField]
    private GameObject _clickAudio;

    private Rigidbody2D _rigidBody;

    private float _xInput;
    private float _yInput;
    private float _mouseX;

    private int _smallPopUpValue = 10;
    private int _mediumPopUpValue = 10;
    private int _bigPopUpValue = 2;

    RaycastHit2D hit;


    private void Awake()
    {

        _rigidBody = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        Cursor.lockState = CursorLockMode.Locked;

        ClosePopUp();

    }

    private void FixedUpdate()
    {
        Move();

    }

    public void ClosePopUp()
    {
        hit = Physics2D.Raycast(transform.position, transform.right, _rayDistance);
        if (hit.collider.isTrigger)
        {
            var _popUpToDestroy = hit.transform.GetComponent<WindowController>()._popUpSize;

            if (Input.GetMouseButtonDown(0)) //Left Mouse Button
            {
                Destroy(hit.transform.gameObject);
                Instantiate(_clickAudio, transform.position, transform.rotation);
                if (_popUpToDestroy == 0)
                {
                    GameManager._score += _smallPopUpValue;
                }
                if (_popUpToDestroy == 1)
                {
                    GameManager._score += _mediumPopUpValue;
                }
                if (_popUpToDestroy == 2)
                {
                    GameManager._score -= _bigPopUpValue;
                }
            }
        }
    }

    private void Move()
    {

        _yInput = Input.GetAxis("Vertical") * _moveSpeed; //Move using W-S or arrow up - arrow down
        _mouseX = Input.GetAxis("Mouse X") * _turnSpeed; // Rotate using mouse.

        transform.Rotate(Vector3.forward, -_mouseX * Time.deltaTime);
        _rigidBody.velocity = transform.right * _yInput;

    }

}
