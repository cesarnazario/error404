﻿using System;
using UnityEngine;

public class WindowController : MonoBehaviour
{
    [SerializeField]
    private GameObject[] _popUpPrefabs;

    private int _currentSize;
    private GameObject _displayPopUp;

    public int _popUpSize = 0;

    private void Start()
    {
        _currentSize = 0;
        _displayPopUp = Instantiate(_popUpPrefabs[_popUpSize], transform.position, transform.rotation, transform);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        AIController _badCursor = other.GetComponent<AIController>();



        //check if the object in our trigger has a AIController component...
        if (_badCursor != null)
        {
            if (_badCursor._currentPopUp.transform.position == transform.position) //To avoid that the bad cursor maximize another window by accident
            {

                if (_popUpSize < _popUpPrefabs.Length - 1)//Maximize only the windows that are maximizable.
                {
                    Destroy(_displayPopUp);
                    _popUpSize++;
                    Resize();
                    _badCursor._lastPopUp = _badCursor._currentPopUp;
                    _badCursor.LookForPopUp();
                    GameManager._score--;
                }
                else
                    _badCursor.LookForPopUp();
            }

        }
    }
    private void Resize()
    {
        _displayPopUp = Instantiate(_popUpPrefabs[_popUpSize], transform.position, transform.rotation, transform);
    }
}
