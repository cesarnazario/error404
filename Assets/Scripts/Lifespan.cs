﻿using UnityEngine;

public class Lifespan : MonoBehaviour
{
    [SerializeField]
    private float _lifespan = 3f;

    private void Start()
    {
        Destroy(gameObject, _lifespan);

    }
}
