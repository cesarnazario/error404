﻿using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using System;

public class ButtonController : MonoBehaviour
{

    private Scene _currentScene;

    private void Awake()
    {
        _currentScene = SceneManager.GetActiveScene();

    }

    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}
