﻿using System;
using UnityEngine;

public class AIController : MonoBehaviour
{

    [SerializeField]
    public WindowController _currentPopUp;

    private WindowController[] _popUpsInScene;
    public WindowController _lastPopUp;
    private int _randomSelection;


    void Update()
    {
        _popUpsInScene = FindObjectsOfType<WindowController>();

        if (_currentPopUp == null)
        {
            LookForPopUp();
        }
        if (_currentPopUp != null)
        {
            if (_lastPopUp == null)
            {
                MoveToPopUp();
            }
            else if (_lastPopUp.transform.position == _currentPopUp.transform.position)
            {
                LookForPopUp();
            }
            else
            {
                MoveToPopUp();
            }
        }
    }


    public void LookForPopUp() //Selection of the popup window to maximize
    {

        transform.position = Vector3.Lerp(transform.position, new Vector2(0, 0), Time.deltaTime / 2); //After it maximize one popup window it should go back to origin.
        _randomSelection = UnityEngine.Random.Range(0, _popUpsInScene.Length);
        _currentPopUp = _popUpsInScene[_randomSelection];
    }
    private void MoveToPopUp() //Once the selection has been made then move towards the popup.
    {

        transform.position = Vector3.Lerp(transform.position, _currentPopUp.transform.position, Time.deltaTime / 2);

    }
}
