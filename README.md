Error 404
====
A 2D Videogame where you are the cursor on your computer battling to close a bunch of windows popping and threaten to crash the computer.
You have an enemy, another cursor that will maximize all the windows it can. Try to beat him!

Features
--------
- Phisycs based character
 - You are able to move the character forward and backword using UP and DOWN arrow keys as well as W and S keys.
 - The rotation gets a little tricky since you have to use your mouse to rotate. This combination brings an extra challengue to the game.

- Pop Up Windows
 - During playtime pop up windows will appear on random places in the screen, your goal is to close them but you can only approach them through the top side, you cannot go over them.
 - If you get to close the pop up before your enemy maximize it you will get 10 points.
 - If you close the pop up with one maximize done. you will get 5 points.
 - If you let the enemy do a second maximize before you close it, you will get substracted 2 points.

- Enemy
 - A second cursor will be on screen with you but instead of helping you it will try to maximize the pop up windows, this will make it more difficult to you.
 - The enemy can move over all the windows but it moves slower than you so try to beat it.
 - If you let the enemy maximize one window you will get 1 point substracted.

- DarkMode(On Development)
 - The hability to invert colors on the game so it is easier on your eyes.